import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';

export default function App() {

  const [aleatoire,setAleatoire] = useState(Math.round(Math.random() * (10 - 0) + 0));
  // setAleatoire(Math.round(aleatoire));

  const [nombreSaisie,setNomreSaisie] = useState("");
  const [nombreTentative,setNombreTentative] = useState(0);
  const [Statut_reponse,setStatut_reponse] = useState("");

  const verification = () => 
  {
    if(nombreSaisie == aleatoire)
    {
      // je reinitialise les champs là par leur valeur de départ
      setNomreSaisie("");
      setStatut_reponse("");
      setNombreTentative(0);
      setAleatoire(Math.round(Math.random() * (10 - 0) + 0));

      setStatut_reponse("Bonne réponse");

      //reinitalisation apres 5 secondes de bonne reponse
      setTimeout(() => {
        setStatut_reponse("")
      }, 5000);

      console.log("ok");;
      return "ok";
    }
    else if(nombreSaisie > aleatoire)
    {
      console.log("moins");
      setNombreTentative(nombreTentative+1);
      console.log("nombreTentative"+nombreTentative);
      setStatut_reponse("C'est moins");
      return "moins";
    }
    else if(nombreSaisie < aleatoire)
    {
      console.log("plus");
      setNombreTentative(nombreTentative+1);
      console.log("nombreTentative"+nombreTentative);
      setStatut_reponse("C'est plus");
      return "plus";
    }
  };

  const NombreSaisie = (text) => 
  {
    setNomreSaisie(text);
    console.log(nombreSaisie);
  };

  const Validation = (text) => 
  {
    console.log("Validation");
    console.log(nombreSaisie);
    console.log(aleatoire);
    verification();
  };

  return (
    <View style={styles.container}>

      <Text>Mon app</Text>
        
      <View name="n_tentatives" style={styles.tentatives}>
        <Text>nombreTentative : {nombreTentative}</Text>
      </View>

      <View name="saisie" style={styles.saisie}>
        <TextInput
          onChangeText={NombreSaisie}
          value={nombreSaisie}
          style={{padding: 10}}
        >
        </TextInput>
      </View>

      <Button
        onPress={Validation}
        title="Confirmer"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />

      <View name="message_rep">
          <Text> {Statut_reponse}</Text>
      </View>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tentatives: {
    backgroundColor: '#aeaeee',
  },
  saisie: {
    backgroundColor: '#cccccc',
  },
});
